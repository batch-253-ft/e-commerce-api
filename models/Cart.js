const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema(
  {
    userId: {
      type: String,
  },
  products: [{
      productId: {
          type: String,
          required: true
      },
      productName: {
        type: String,
        required: true
      },
      imageUrl: {
        type: String,
        required: true
      },
      quantity: {
          type: Number,
          required: true,
          min: [1, 'Quantity can not be less then 1.'],
          default: 1
      },
      price: {
        type: Number,
        required: true  
      },
      subtotal: {
        type: Number
      },
      addedOn: {
        type: Date,
        default: new Date()
      }

  }],
  totalAmount: {
      type: Number
    
  },
  purchasedOn: {
    type: Date,
    default: new Date()
  },
  status: {
    type: String,
    default: "pending"
  }
});

cartSchema.pre('save', function(next) {
  let totalAmount = 0;
  this.products.forEach((product) => {
    product.subtotal = Math.round(product.price * product.quantity);
    totalAmount += product.subtotal;
  });
  this.totalAmount = totalAmount; 
  next();
});

module.exports = mongoose.model("Cart", cartSchema);
