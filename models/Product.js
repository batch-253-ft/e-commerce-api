const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({

    productName : { 
        type : String, 
        required : [true, "Title is required."] 
    },
    description : { 
        type : String, 
        required : [true, "Description is required."] 
    },
    
    price : { 
        type : Number, 
        required : [true, "Price is required."] 
    },
    isActive : {
        type : Boolean,
        default : true
    },
    imageUrl: {
        type: String,
        required: [true, "Image URL is required."]
    }
    // ,
    // userOrders : 
    //     [{
    //         userId: {
    //             type : String, 
    //         required : true 
    //         },
    //         createdOn: {
	// 			type: Date,
	// 			default: new Date()
	// 		}
           
    //     }]
    
    
},
{ timestamps: true }
)

module.exports = mongoose.model("Product", productSchema);