const express = require("express");
const router = express.Router();

const auth = require("../auth");
const cartController = require("../controllers/cartController");

router.post("/:id", (req, res) => {
    cartController.addToCartProduct(req.body)
    .then(resultFromController => res.send(resultFromController))
    .catch(err => res.send(err))
});


module.exports = router;