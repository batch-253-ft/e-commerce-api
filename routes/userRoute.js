const express = require("express");
const router = express.Router();

const userController = require("../controllers/userController");
const auth = require("../auth");

// ======================== Register User ========================
router.post("/register", (req, res) => {
    userController.userRegistration(req.body)
    .then(resultFromController => res.send(resultFromController))
    .catch(err => res.send(err))
});

// ======================== Login User =========================

router.post("/login", (req, res) => {
    userController.loginUser(req.body)
    .then(resultFromController => res.send(resultFromController))
    .catch(err => res.send(err));
});

// =============== Get All Users (Admin only) ===================
router.get("/allUser", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    if(userData.isAdmin) {
        userController.getAllUsers({ userId : userData.id})
        .then(resultFromController => res.send(resultFromController))
        .catch(err => res.send(err))
    } else {
        res.send(false);
    }
});
// =============== Get Single User ===================
router.get("/details", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    let data ={
        userId: userData.id
    }
    
        userController.getSingleUser(data)
        .then(resultFromController => res.send(resultFromController))
        .catch(err => res.send(err))
   
});

// ==================== Update User role (admin only) =====================
router.patch("/:userId/updateUserRole", auth.verify, (req, res) =>{
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin) {
        userController.updateUserRole(req.params, req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
    } else {
        res.send(false);
    };
})

// ======================= Create Order ====================
router.post("/createOrder", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    let data = {
        userId : userData.id,
        productId: req.body.productId,
        productName: req.body.productName,
        price: req.body.price,
        quantity: req.body.quantity
        

    }
    if(!userData.isAdmin) {
        userController.createOrder(data).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));

    } else {
        res.send(false);
    }

});

// =================== View user Cart =========================
router.get("/:userId/myCart", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    
    let data ={
        userId: userData.id
    }

    if(!userData.isAdmin) {
        userController.viewMyCart(data).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
    } else {
        res.send(false);
    }
});

// =================== View all my orders =========================
router.get("/allUserOrder", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    
    let data ={
        userId: userData.id
    }

    if(!userData.isAdmin) {
        userController.viewAllOrder(data).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
    } else {
        res.send(false);
    }
});

// ==================== Check Out Order from Cart ===================
router.patch("/:userId/checkOut", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    
    if(!userData.isAdmin) {
        userController.checkOut(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
    } else {
        res.send(false);
    }

})

// ================== Add item to cart =============
router.post("/addItemToCart", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    
    if(!userData.isAdmin) {
        userController.addItemToCart(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
    } else {
        res.send(false);
    };
})

// ================= view all users cart =============
router.get("/viewAllCart", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    
    if(userData.isAdmin) {
        userController.viewAllCart().then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
    } else {
        res.send(false);
    };
})

// ================= view all checked out cart =============
router.get("/checkedOutCart", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    
    if(userData.isAdmin) {
        userController.checkedOut().then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
    } else {
        res.send(false);
    };
})


// ================= change quantity of product to cart =============
router.patch("/changeCartQuantity", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    
    if(!userData.isAdmin) {
        userController.changeQuantity(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
    } else {
        res.send(false);
    };
})

// ================= change quantity of product to cart =============
router.patch("/deleteItemOnCart", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    
    if(!userData.isAdmin) {
        userController.deleteItemOnCart(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
    } else {
        res.send(false);
    };
})

// ==================== Buy now  ===================
router.post("/buyNow", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    
    if(!userData.isAdmin) {
        userController.buyNow(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
    } else {
        res.send(false);
    }

})

// ==================== Update User Profile =====================
router.patch("/updateUserProfile", auth.verify, (req, res) =>{
    const userData = auth.decode(req.headers.authorization);

    userController.updateUserProfile(req.body)
    .then(resultFromController => res.send(resultFromController))
    .catch(err => res.send(err))
})





module.exports = router;