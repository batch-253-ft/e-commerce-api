const bcrypt = require("bcrypt");
const auth = require("../auth");

const User = require("../models/User");
const Product = require("../models/Product");
const Cart = require("../models/Cart");


// =================== User Registration =======================
module.exports.userRegistration = (reqBody) => {
    return User.findOne({ email : reqBody.email}).then(result => {
        if(result){
            //check if email is registered
            return false;
        } else {
            let newUser = new User({
                firstName: reqBody.firstName,
                lastName: reqBody.lastName,
                mobileNum: reqBody.mobileNum,
                email: reqBody.email,
                password: bcrypt.hashSync(reqBody.password, 10)
            });
            return newUser.save().then(user => {

                if(user){
                    return true
                } else {
                    return false
                }
            }).catch(err => {
                console.log(err)
                return false
            })  
        }
    }).catch(err => {
        console.log(err)
        return false
    })
};

// =================== LOGIN USER =======================
module.exports.loginUser = (reqBody) => {
    return User.findOne({ email : reqBody.email}).then(result => {
        if(result == null){
            return false
        } else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
            if(isPasswordCorrect){
                return { 
                    status : "Log in successful. See below access Token and UserId.",
                    access : auth.createAccessToken(result),
                    userId : result.id 
                }
                
            } else {
                return false
            }
        }
    }).catch(err => {
        console.log(err)
        return false
    });
}

// =============== Get All Users ==================
module.exports.getAllUsers = (data) => {
    return User.find({}).then(result => {
        return result;
    }).catch(err => err);
};

// =============== Get Single User ==================
module.exports.getSingleUser = (data) => {
    return User.findById(data.userId).then(result => {
        // show empty string on password
        result.password = "";
        return result;
    }).catch(err => err);
};

// ================= Update User Role (admin only) ==================
module.exports.updateUserRole = (reqParams, reqBody) => {
    let updatedUserRole = {
        isAdmin: reqBody.isAdmin,   
    };
    return User.findByIdAndUpdate(reqBody.userId, updatedUserRole)
    .then(result=> true)
    .catch(err => {
        console.log(err)
        return false
    });
};

// =========================== Create Order ===============================
module.exports.createOrder = (data) => {
    let {userId, productId, productName, imageUrl, price, quantity} = data;
    // check if product is existing
    return Product.findOne({productName: productName})
        .then(result => {
            if(result){
                // check if cart is existing
                return Cart.findOne({userId : userId})
                    .then(userCart => {
                        // if existing, check if the cart is already checked out
                        if(userCart){
                            const { cartId, userId, products, purchasedOn, status, totalAmount} = userCart;
                            console.log(status);
                            // if already checked out, create new Cart
                            if(status === "Check Out"){
                                let newCart = new Cart({
                                    userId : userId,
                                    products: [{
                                        productId : productId,
                                        productName : productName,
                                        imageUrl: imageUrl,
                                        price : price,
                                        quantity : quantity
                                        
                                    }]
                                })
                                return newCart.save().then(cart => {
                                    if(cart){
                                        return true
                                    } else {
                                        return false
                                    }
                                }).catch(err => err)
                            } else {
                                return false
                            }
                        // create new Cart
                        } else {
                            let newCart = new Cart({
                                userId : userId,
                                products: [{
                                    productId : productId,
                                    productName : productName,
                                    imageUrl: imageUrl,
                                    price : price,
                                    quantity : quantity
                                    
                                }]
                            })
                            return newCart.save().then(cart => {
                                if(cart){
                                    return true
                                } else {
                                    return false
                                }
                            }).catch(err => err)
                        }
                    }).catch(err => err)
                
            } else {
                return false

            }
        })
  
};

// ================ View My Cart ===================
module.exports.viewMyCart = (data) => {
    return Cart.findOne({
        userId: data.userId, 
        status: "pending"
    }).then(result => {
        console.log(data.userId)
        console.log(result)
        return result;
    }).catch(err => err);
};

// ================ View All my Orders ===================
module.exports.viewAllOrder = (data) => {
    return Cart.find({
        userId: data.userId, 
        status: "Check Out"
    }).then(result => {
        console.log(data.userId)
        console.log(result)
        return result;
    }).catch(err => err);
};


// ====================== Check Out Cart =================

module.exports.checkOut = async (reqBody) => {
    try {
      const cart = await Cart.findById(reqBody.cartId);
      if (!cart) {
        throw new Error('Cart not found');
      }
      cart.status = 'Check Out';
      const updatedCart = await cart.save();
      console.log(updatedCart);
      return true;
    } catch (err) {
      console.error(err);
      throw err;
    }
  };

// ======================= add item to cart =======================

module.exports.addItemToCart = async (reqBody) => {
    try {
      const { userId, productId, productName, imageUrl, price, quantity } = reqBody;
      const userCart = await Cart.findOne({ userId: userId, status: "pending" });
      if (userCart) {
        // check if product is already existing on cart
        const existingProduct = userCart.products.find((p) => p.productName === productName);
        if (existingProduct) {
          existingProduct.quantity += quantity;
        } else {
          userCart.products.push({
            productId: productId,
            productName: productName,
            imageUrl: imageUrl,
            price: price,
            quantity: quantity,
          });
        }
        const result = await userCart.save();
        return true;
      } else {
        const newCart = new Cart({
          userId: userId,
          products: [
            {
              productId: productId,
              productName: productName,
              imageUrl: imageUrl,
              price: price,
              quantity: quantity,
            },
          ],
          status: "pending",
        });
        const cart = await newCart.save();
        return true;
      }
    } catch (error) {
      return error;
    }
  };




// ================== view all cart (admin only) ===================
module.exports.viewAllCart = (reqBody) => {
    return Cart.find({}).then(result => {
        return result;
    }).catch(err => err);
};

// ================== view all checked Out cart (admin only) ===================
module.exports.checkedOut = (reqBody) => {
    return Cart.find({status : "Check Out"}).then(result => {
        if(result) {
            return result;
        } else {
            return false;
        }       
    }).catch(err => {
        console.log(err)
        return false
    });
};

// =============== Change quantity of product =================
module.exports.changeQuantity = (reqBody) => {
    let {cartId, productId, productName, quantity} = reqBody;
    return Cart.findById(cartId)
        .then(userCart => {
            if(userCart){
                userCart.products.find( p => 
                    p.productName === productName && ( p.quantity = quantity, true ) );
                return userCart.save().then(result => true)
                .catch(err => err)
            } else {
                return false
            }
        }).catch(err => {
            console.log(err)
            return false
        })
    };

// ======================= delete item to cart =======================
module.exports.deleteItemOnCart = (reqBody) => {
    let {cartId, productName } = reqBody;
    return Cart.findById(cartId)
        .then(userCart => {
            if(userCart){
                userCart.products.splice(
                    userCart.products.findIndex(item => 
                        item.productName === productName
                    ), 1
                )
                return userCart.save().then(result => true).catch(err => err);
            } else {
                return false;
            }
        }).catch(err => {
            console.log(err)
            return false
        });
};


// =========================== Buy Now ===============================
module.exports.buyNow = (reqBody) => {
    let {userId, productId, productName, imageUrl, price, quantity} = reqBody;
    // check if product is existing
    return Product.findOne({productName: productName})
        .then(result => {
            if(result){
                // create checked out Cart
                let newCart = new Cart({
                    userId : userId,
                    products: [{
                        productId : productId,
                        productName : productName,
                        imageUrl: imageUrl,
                        price : price,
                        quantity : quantity
                        
                    }],
                    status: "Check Out"
                })
                return newCart.save().then(cart => {
                    if(cart){
                        return true
                    } else {
                        return false
                    }
                }).catch(err => err)
            } else {
                return false
            }
        })
}

// ============ Update User Profile ==========
module.exports.updateUserProfile = (reqBody) => {
    let updatedUserRole = {
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNum: reqBody.mobileNum,
        password: bcrypt.hashSync(reqBody.password, 10)  
    };
    return User.findByIdAndUpdate(reqBody.userId, updatedUserRole)
    .then(result=> true)
    .catch(err => {
        console.log(err)
        return false
    });
};